var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

gulp.task('html', function() {
	return new Promise(function(resolve, reject) {
		browserSync.reload();
		resolve();
	});	
});

gulp.task('sass', function() {
	return new Promise(function(resolve, reject) {
		gulp.src('src/scss/*.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('public/css/'))
		.pipe(browserSync.stream());
		resolve();
	});	
});

gulp.task('browserSync', function() {
	return new Promise(function(resolve, reject) {
		browserSync({
			server: {
				baseDir: './'
			}
		});
		resolve();
	});
});

gulp.task('watch', function() {
	gulp.watch(['src/scss/*.scss'], gulp.series('sass'));
	return gulp.watch(['./*.html', 'build/js/*.js'], gulp.series('html'));
})

gulp.task('default', gulp.series('browserSync', 'html', 'sass', 'watch'));
// 	return new Promise(function(resolve, reject) {
// 		console.log('shit');
		
// 		resolve();
// 	});
// });

