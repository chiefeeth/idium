setTimeout(function () {
    let viewheight = window.visualViewport.height
    let viewwidth = window.visualViewport.width
    let viewport = document.querySelector("meta[name=viewport]");
    viewport.setAttribute("content", "height=" + viewheight + "px, width=" + viewwidth + "px, initial-scale=1.0");
}, 300);

document.addEventListener('DOMContentLoaded', function() {
    var url = window.location.href;
    var id = url.substring (url.lastIndexOf('/') + 1);
    console.log(id, url);
    if (id == '#form-approved') {
        alert('Your submission has been recoded. Thank you!')
    };
    if (id == '#form-error') {
        alert('Error. Please fill-out every field')
    };
});
